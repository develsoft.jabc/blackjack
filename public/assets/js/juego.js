(() => {
  //Arreglos y variables
  ("use strict");

  let deck = [];
  let plantar1 = false;
  let plantar2 = false;
  let card2;
  let v;
  let pedir;
  let pointsJ = 0;
  let pointsPC = 0;

  const palos = ["C", "H", "S", "D"],
        letras = ["A", "J", "Q", "K"];

  //Arreglos

  //Mensajes
  const result = document.querySelector("#gano");
    //Mensajes

  //HTML
  const newGame = document.querySelector("#newGame"),
        pJ = document.querySelector("#pJ"),
        pPC = document.querySelector("#pPC"),
        askCard = document.querySelector("#newCard"),
        btnPlantar1 = document.querySelector("#plantar1"),
        pedirC = document.querySelector("#player-cards"),
        pedirPC = document.querySelector("#pc-cards"),
        back = document.querySelector("#back");

  askCard.disabled = true;
  btnPlantar1.disabled = true;

  //HTML
  //Eventos

  //Buton Nuevo Juego
  newGame.addEventListener("click", () => {
    // console.clear();
    newDeck();
    limpiarPantalla();

  });

  //Boton Pedir carta
  askCard.addEventListener("click", () => {
    
    btnPlantar1.disabled = false;
    pedir = Math.round(Math.random() * 1);

   
      let card1 = newCard();
      pointsJ += valueCard(card1, pointsJ);

      const img1 = document.createElement("img");
      img1.src = `/assets/cartas/${card1}.png`;
      pedirC.append(img1);

      pJ.innerText = pointsJ;
    

    if (plantar2 == false) {
      mostrarCartasPC();

      if (pointsPC >= 15) {
        if (pedir == 0) {
          plantar2 = true;
        }
      }
    }

    ganador(pointsJ, pointsPC);
  });

  //Botón Plantarse

  btnPlantar1.addEventListener("click", () => {

    plantar1 = true;
    btnPlantar1.disabled = true;
    askCard.disabled = true;

    while (pedir == 1 && pointsPC <= 17 && plantar2 == false) {
      mostrarCartasPC();
      pedir = Math.round(Math.random() * 1);
    }

    plantar2 = true;

    if (plantar1 == true && plantar2 == true) {

      let resta1 = 21 - pointsJ;
      let resta2 = 21 - pointsPC;

      if ((resta1 < resta2 && pointsJ <= 21) || pointsPC > 21) {
        crearMsj("Ganaste", "success");
      } else if ((resta2 < resta1 && pointsPC <= 21) || pointsJ > 21) {
        crearMsj("Perdiste", "danger");
      } else if (pointsJ == pointsPC) {
        crearMsj("Empate", "warning");
      } else {
        crearMsj("No hay ganador", "danger");
      }
    }

  });

  //Eventos

  //Funciones
  //Nuevo mazo
  const newDeck = () => {
    deck = [];

    for (let i = 2; i <= 10; i++) {
      for (let j in palos) {
        deck.push(i + palos[j]);
      }
    }

    for (let letra of letras) {
      for (let palo of palos) {
        deck.push(letra + palo);
      }
    }

    deck = _.shuffle(deck);

    return deck;
  };

  //Pedir carta
  const newCard = () => {
   
      return deck.pop();
    
  };

  //Valor de la carta
  const valueCard = (card, points) => {
    const value = card.substring(0, card.length - 1);
    let num = 0;

    if (points <= 10 && value === "A") {
      num = 11;
    } else {
      num = ["J", "Q", "K"].includes(value)
        ? 10
        : value === "A"
        ? 1
        : value * 1;
    }

    return num;
  };

  //Imprimir las cartas del jugador 2
  const mostrarCartasPC = () => {
    card2 = newCard();
    v = valueCard(card2, pointsPC);
    pointsPC += v;

    const img2 = document.createElement("img"),
          img3 = document.createElement("img");
    img2.src = `/assets/cartas/${card2}.png`;
    img3.src = `/assets/cartas/grey_back.png`;

    pedirPC.append(img2);
    back.append(img3);

    pPC.innerText = "- " + pointsPC;
  };

  const limpiarPantalla = () => {
    pointsJ = pointsPC = 0;
    pedirC.innerHTML = "";
    pedirPC.innerHTML = "";
    back.innerHTML = "";
    pJ.innerText = 0;
    pPC.innerText = 0;
    askCard.disabled = false;
    btnPlantar1.disabled = true;
    plantar1 = plantar2 = false;
    result.innerHTML = "";
    pedirPC.classList.add("hidden");
    back.classList.remove("hidden");
    pPC.classList.add("hidden");
  };

  const ganador = (pointsJ, pointsPC) => {
    if (pointsJ == 21 && pointsPC == 21) {
      askCard.disabled = true;
      btnPlantar1.disabled = true;

      crearMsj("Empate", "warning");
    } else if (pointsJ == 21) {
      askCard.disabled = true;
      btnPlantar1.disabled = true;

      crearMsj("Ganaste", "success");
    } else if (pointsPC == 21) {
      askCard.disabled = true;
      btnPlantar1.disabled = true;

      crearMsj("Perdiste", "danger");
    } else if (pointsJ > 21 && pointsPC <= 21) {
      askCard.disabled = true;
      btnPlantar1.disabled = true;

      crearMsj("Perdiste", "danger");
    } else if (pointsPC > 21 && pointsJ <= 21) {
      askCard.disabled = true;
      btnPlantar1.disabled = true;

      crearMsj("Ganaste", "success");
    } else if (pointsPC > 21 && pointsJ > 21) {
      crearMsj("No hay ganador", "danger");
    }
  };

  const crearMsj = (msj, clase) => {
    const gano = document.createElement("div");

    gano.setAttribute("class", `alert alert-${clase}`);
    gano.setAttribute("role", "alert");
    gano.innerText = msj;

    pedirPC.classList.remove("hidden");
    back.classList.add("hidden");
    pPC.classList.remove("hidden");

    result.append(gano);
  };

  //Funciones
})();