import { crearMsj } from "./crear-msj";


export const ganador = (pointsJ, pointsPC, askCard, btnPlantar1, pedirPC, back, pPC, result) => {
    if (pointsJ == 21 && pointsPC == 21) {
      askCard.disabled = true;
      btnPlantar1.disabled = true;

      crearMsj("Empate", "warning", pedirPC, back, pPC, result);
    } else if (pointsJ == 21) {
      askCard.disabled = true;
      btnPlantar1.disabled = true;

      crearMsj("Ganaste", "success", pedirPC, back, pPC, result);
    } else if (pointsPC == 21) {
      askCard.disabled = true;
      btnPlantar1.disabled = true;

      crearMsj("Perdiste", "danger", pedirPC, back, pPC, result);
    } else if (pointsJ > 21 && pointsPC <= 21) {
      askCard.disabled = true;
      btnPlantar1.disabled = true;

      crearMsj("Perdiste", "danger", pedirPC, back, pPC, result);
    } else if (pointsPC > 21 && pointsJ <= 21) {
      askCard.disabled = true;
      btnPlantar1.disabled = true;

      crearMsj("Ganaste", "success", pedirPC, back, pPC, result);
    } else if (pointsPC > 21 && pointsJ > 21) {
      crearMsj("No hay ganador", "danger", pedirPC, back, pPC, result);
    }
  };