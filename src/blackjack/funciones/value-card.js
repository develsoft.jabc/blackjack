//Valor de la carta
export const valueCard = (card, points) => {
    const value = card.substring(0, card.length - 1);
    let num = 0;

    if (points <= 10 && value === "A") {
      num = 11;
    } else {
      num = ["J", "Q", "K"].includes(value)
        ? 10
        : value === "A"
        ? 1
        : value * 1;
    }

    return num;
  };