


export const crearMsj = (msj, clase, pedirPC, back, pPC, result) => {
    const gano = document.createElement("div");

    gano.setAttribute("class", `alert alert-${clase}`);
    gano.setAttribute("role", "alert");
    gano.innerText = msj;

    pedirPC.classList.remove("hidden");
    back.classList.add("hidden");
    pPC.classList.remove("hidden");

    result.append(gano);
  };