//Imprimir las cartas del jugador 2
import { newCard } from "./new-card";
import { valueCard } from "./value-card";

export const mostrarCartasPC = (deck, card2, pointsPC, pedirPC, back, pPC) => {
  card2 = newCard(deck);

  pointsPC += valueCard(card2, pointsPC);


  const img2 = document.createElement("img"),
    img3 = document.createElement("img");
    img2.src = `/assets/cartas/${card2}.png`;
    img3.src = `/assets/cartas/grey_back.png`;

  pedirPC.append(img2);
  back.append(img3);

  pPC.innerText = "- " + pointsPC;

  return pointsPC;
};
