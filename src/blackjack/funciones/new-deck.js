import _ from 'underscore';


/**
 * 
 * @param {Array<string>} palos ["C", "H", "S", "D"],
 * @param {Array<string>} letras ["A", "J", "Q", "K"];
 * @returns {Array<string>} retorna un arreglo del mazo
 */
export const newDeck = (palos, letras) => {

    if (!palos || palos.length === 0) throw new Error('Arreglo palos obligatorio');

    if (!letras || letras.length === 0) throw new Error('Arreglo letras obligatorio');

    let deck = [];

    for (let i = 2; i <= 10; i++) {
      for (let j in palos) {
        deck.push(i + palos[j]);
      }
    }

    for (let letra of letras) {
      for (let palo of palos) {
        deck.push(letra + palo);
      }
    }

    deck = _.shuffle(deck);

    return deck;
  };

//   export default newDeck;