/**
 * @returns {Array} retorna una arreglo de valores para reiniciar las variables de puntos de los jugadores y plantarse
 */

export const limpiarPantalla = (pointsJ, pointsPC, pedirC, pedirPC, back, pJ, pPC, askCard, btnPlantar1, plantar1, plantar2, result) => {
    pointsJ = pointsPC = 0;
    pedirC.innerHTML = "";
    pedirPC.innerHTML = "";
    back.innerHTML = "";
    pJ.innerText = 0;
    pPC.innerText = 0;
    askCard.disabled = false;
    btnPlantar1.disabled = true;
    plantar1 = plantar2 = false;
    result.innerHTML = "";
    pedirPC.classList.add("hidden");
    back.classList.remove("hidden");
    pPC.classList.add("hidden");
    return [pointsJ, pointsPC, plantar1, plantar2];
  };