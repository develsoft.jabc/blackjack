import _ from "underscore";
import { newDeck } from "./funciones/new-deck";
import { newCard } from "./funciones/new-card";
import { valueCard } from './funciones/value-card';
import { mostrarCartasPC } from "./funciones/mostrar-cards-pc";
import { limpiarPantalla } from "./funciones/limpiar-pantalla";
import { ganador } from "./funciones/ganador";
import { crearMsj } from "./funciones/crear-msj";

//Arreglos y variables
(() => {
  "use strict";
  let deck = [];
  let plantar1 = false;
  let plantar2 = false;
  let card2;
  let pedir;
  let pointsJ = 0;
  let pointsPC = 0;

  //Arreglos
  const palos = ["C", "H", "S", "D"],
        letras = ["A", "J", "Q", "K"];

  //Mensajes
  const result = document.querySelector("#gano");
  //Mensajes

  //HTML
  const newGame = document.querySelector("#newGame"),
        pJ = document.querySelector("#pJ"),
        pPC = document.querySelector("#pPC"),
        askCard = document.querySelector("#newCard"),
        btnPlantar1 = document.querySelector("#plantar1"),
        pedirC = document.querySelector("#player-cards"),
        pedirPC = document.querySelector("#pc-cards"),
        back = document.querySelector("#back");

        askCard.disabled = true;
        btnPlantar1.disabled = true;

  //HTML
  //Eventos

  //Buton Nuevo Juego
  newGame.addEventListener("click", () => {
    // console.clear();
    deck = newDeck(palos, letras);

   pointsJ = limpiarPantalla(pointsJ, pointsPC, pedirC, pedirPC, back, pJ, pPC, askCard, btnPlantar1, plantar1, plantar2, result)[0];

   pointsPC = limpiarPantalla(pointsJ, pointsPC, pedirC, pedirPC, back, pJ, pPC, askCard, btnPlantar1, plantar1, plantar2, result)[1];

   plantar1 = limpiarPantalla(pointsJ, pointsPC, pedirC, pedirPC, back, pJ, pPC, askCard, btnPlantar1, plantar1, plantar2, result)[2];

   plantar2 = limpiarPantalla(pointsJ, pointsPC, pedirC, pedirPC, back, pJ, pPC, askCard, btnPlantar1, plantar1, plantar2, result)[3];
  });

  //Boton Pedir carta
  askCard.addEventListener("click", () => {

    btnPlantar1.disabled = false;
    pedir = Math.round(Math.random() * 1);

    let card1 = newCard(deck);
    pointsJ += valueCard(card1, pointsJ);

    const img1 = document.createElement("img");
          img1.src = `/assets/cartas/${card1}.png`;
          pedirC.append(img1);

          pJ.innerText = pointsJ;

    if (plantar2 == false) {
      
      pointsPC = mostrarCartasPC(deck, card2, pointsPC, pedirPC, back, pPC);

      if (pointsPC >= 15) {
        if (pedir == 0) {
          plantar2 = true;
        }
      }
    }

    ganador(pointsJ, pointsPC, askCard, btnPlantar1, pedirPC, back, pPC, result);
  });

  //Botón Plantarse

  btnPlantar1.addEventListener('click', () => {
    plantar1 = true;
    btnPlantar1.disabled = true;
    askCard.disabled = true;

    while (pedir == 1 && pointsPC <= 17 && plantar2 == false) {
      pointsPC = mostrarCartasPC(deck, card2, pointsPC, pedirPC, back, pPC);
      pedir = Math.round(Math.random() * 1);
    }

    plantar2 = true;

    if (plantar1 == true && plantar2 == true) {
      let resta1 = 21 - pointsJ;
      let resta2 = 21 - pointsPC;

      if ((resta1 < resta2 && pointsJ <= 21) || pointsPC > 21) {
        crearMsj("Ganaste", "success", pedirPC, back, pPC, result);
      } else if ((resta2 < resta1 && pointsPC <= 21) || pointsJ > 21) {
        crearMsj("Perdiste", "danger", pedirPC, back, pPC, result);
      } else if (pointsJ == pointsPC) {
        crearMsj("Empate", "warning", pedirPC, back, pPC, result);
      } else {
        crearMsj("No hay ganador", "danger", pedirPC, back, pPC, result);
      }
    }
  });

  //Eventos
})();